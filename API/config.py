"""
Config project
"""

fd_apikey = "111"

class GeojsonValidationConfiguration:
    point = 'Point'
    multi_point = 'MultiPoint'
    line_string = 'LineString'
    multi_line_string = 'MultiLineString'
    polygon = 'Polygon'
    multi_polygon = 'MultiPolygon'
    feature = 'Feature'
    feature_collection = 'FeatureCollection'

    line_ring_const = 'LinearRing'
    line_const = 'Line'

    coordinates_key = 'coordinates'
    type_key = 'type'
    feature_key = 'features'
    geometry_key = 'geometry'

class DatabaseConfiguration:
    """
    Constants used throughout the ORM.
    """
    operator_expression = '$set'
    operator_increment = '$inc'
    many_params_error = 'InvalidManyParam: Must provide bool variable'
    data_params_error = 'InvalidDataParam: Must be dict or list'
    default_params_error = 'InvalidParams: Must be dict'
    replicaset_primary_error = 'Replica set not primary'