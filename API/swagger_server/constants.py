"""
Project's constants
"""


class MongodbConstants:
    """
    SOCKETS - list or tuple of strings
    REPLICA_SET, DATABASE, COLLECTION, USER, PASSWORD - string
    FOR EXAMPLE
    SOCKETS = ['0.0.0.0:27017', '0.0.0.0:27018', '0.0.0.0:27019']
    REPLICA_SET = 'replica_name'
    DATABASE = 'database_name'
    COLLECTION = 'collection_name'
    USER = 'root'
    PASSWORD = 'password'
    """

    # SOCKETS = ['192.168.0.184:27001']
    # SOCKETS = ['0.0.0.0:27017']
    SOCKETS = ['mongodb:27017']
    REPLICA_SET = 'FirstReplica'
    DATABASE = None
    COLLECTION = None
    USER = None
    PASSWORD = None
