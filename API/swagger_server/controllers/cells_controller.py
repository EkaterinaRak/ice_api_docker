import connexion
from datetime import datetime
from swagger_server.orm import OrmMongodb
from swagger_server.controllers.frames_controller import get_cells_frames_by_id as frames
from swagger_server.controllers.main import get_apikey
from config import fd_apikey
from swagger_server.controllers.problem import problem
from swagger_server.validator.geojson_validation import is_valid


database = "collections"
collection = "cells"
db_client = OrmMongodb(collection=collection, database=database)


def get_cells_by_id(id:str) -> dict:
    """
    Return cells in grid with defined id
    :rtype: dict
    """
    isauth = get_apikey()
    if isauth == True:
        result_list = []
        cells_in_grid = db_client.read(data={"grid_id": id}, many=True)
        for item in cells_in_grid:
            cell = {
                "type": item['type'],
                "geometry": item['geometry'],
                "properties": {
                    "id": item['id'],
                    "name": item['name'],
                    "grid_id": item['grid_id'],
                    "last_update": item['last_update']
                }
            }
            result_list.append(cell)
        response = {
            "type": "FeatureCollection",
            "features": result_list
        }
    else:
        response = problem(401, "Unauthorized", "Unauthorized")
    return response


def post_cells_by_id(id:str) -> dict:
    """
    Post a list of grid cells
    :rtype: dict
    """
    isauth = get_apikey()
    if isauth == True:
        if connexion.request.headers['X-Api-Key'] == fd_apikey:
            result_list = []
            cells_to_add = connexion.request.json['features']
            for cell_in_request in cells_to_add:
                start_count = db_client.count_document({"_id": cell_in_request['properties']['id']})
                if start_count == 0:
                    cell_to_add = {
                        "_id": cell_in_request['properties']['id'],
                        "id": cell_in_request['properties']['id'],
                        "grid_id":id,
                        "name": cell_in_request['properties']['name'],
                        "last_update":  "0001-01-01T00:00:00.000Z",
                        "type": cell_in_request['type'],
                        "geometry": cell_in_request['geometry']
                    }
                    isvalid = is_valid(cell_to_add)
                    if isvalid:
                        db_client.insert(cell_to_add)
                        end_count = db_client.count_document({"_id": cell_in_request['properties']['id']})
                        if end_count > start_count:
                            result = {
                                "id": cell_to_add['id'],
                                "name": cell_to_add['name']
                            }
                    else:
                        result = {
                            "name": cell_to_add['name'],
                            "error":"The polygon has an wrong geometry"
                        }
                else:
                    cell_to_add = {
                        "id": cell_in_request['properties']['id'],
                        "grid_id": id,
                        "name": cell_in_request['properties']['name'],
                        "last_update": datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S"),
                        "type": cell_in_request['type'],
                        "geometry": cell_in_request['geometry']
                    }
                    isvalid = is_valid(cell_to_add)
                    if isvalid:
                        cell_upd = db_client.update(data=cell_to_add, by_set={"_id":cell_to_add['id']})
                        if cell_upd.modified_count > 0:
                            result = {
                                "id": cell_to_add['id'],
                                "name": cell_to_add['name']
                            }
                    else:
                        result = {
                            "name": cell_to_add['name'],
                            "error": "The polygon has an wrong geometry"
                        }
                result_list.append(result)
            keys = []
            for i in result_list:
                for key in i.keys():
                    keys.append(key)
            status = ""
            if "error" in keys and "id" in keys:
                status = "partial_success"
            if "id" in keys and "error" not in keys:
                status = "success"
            if "error" in keys and "id" not in keys:
                status = "failure"
            response = {
                "id": id,
                "status": status,
                "operation": "post",
                "description": result_list
            }
        else:
            response = problem(403, "Forbidden", "Forbidden")
    else:
        response = problem(401, "Unauthorized", "Unauthorized")
    return response


def delete_cells_by_id(id:str) -> dict:
    """
    Delete cells in the grid
    :rtype: dict
    """
    isauth = get_apikey()
    if isauth == True:
        if connexion.request.headers['X-Api-Key'] == fd_apikey:
            result_list = []
            cells_in_grid = db_client.read({"grid_id":id}, many=True)
            for cell in cells_in_grid:
                query = {"_id":cell['id']}
                start_count = db_client.count_document(query)
                db_client.delete(query)
                end_count = db_client.count_document(query)
                if start_count > end_count:
                    result={
                        "id":cell['id'],
                        "name": cell['name']
                    }
                else:
                    result={
                        "name":cell['name'],
                        "error": "Something went wrong"
                    }
                result_list.append(result)
            keys = []
            for i in result_list:
                for key in i.keys():
                    keys.append(key)
            status = ""
            if "error" in keys and "id" in keys:
                status = "partial_success"
            if "id" in keys and "error" not in keys:
                status = "success"
            if "error" in keys and "id" not in keys:
                status = "failure"
            response = {
                "id": "33f62123-6bca-4863-9bbb-320302056410",
                "status": status,
                "operation": "post",
                "description": result_list
            }
        else:
            response = problem(403, "Forbidden", "Forbidden")
    else:
        response = problem(401, "Unauthorized", "Unauthorized")
    return response


def get_cells_frames_by_id(id:str) -> dict:
    """
    Get a list of grid cell frames
    :rtype: dict
    """
    isauth = get_apikey()
    if isauth == True:
        cell_ids = []
        cells_in_grid = db_client.read({"grid_id": id}, many = True)
        for cell in cells_in_grid:
            cell_ids.append(cell['_id'])
        response = frames(cell_ids)
    else:
        response = problem(401, "Unauthorized", "Unauthorized")
    return response


def get_cell_by_id(id:str) -> dict:
    """
    Get metadata for a cell
    :rtype: dict
    """
    isauth = get_apikey()
    if isauth == True:
        cell = db_client.read({"_id":id})
        response = {
            "type": "FeatureCollection",
            "features": [
        {
          "type": cell['type'],
          "geometry":cell['geometry'],
          "properties": {
            "id": cell['id'],
            "grid_id":  cell['grid_id'],
            "name": cell['name'],
            "last_update": cell['last_update']
          }
        }
      ]
    }
    else:
        response = problem(401, "Unauthorized", "Unauthorized")
    return response


def put_cell_by_id(id:str) -> dict:
    """
    Add new cell with defined id
    :rtype: dict
    """
    isauth = get_apikey()
    if isauth == True:
        if connexion.request.headers['X-Api-Key'] == fd_apikey:
            response = {"id":id}
            count = db_client.count_document({"_id":id})
            item = connexion.request.json['features'][0]
            isvalid = is_valid(item)
            if isvalid:
                if count == 0:
                    cell_to_add = {
                        "_id": id,
                        "id": id,
                        "type": item['type'],
                        "geometry": item['geometry'],
                        "grid_id": item['properties']['grid_id'],
                        "name": item['properties']['name'],
                        "last_update": "0001-01-01T00:00:00.000Z"
                    }
                    db_client.insert(cell_to_add)
                    end_count = db_client.count_document({"_id":id})
                    if end_count > count:
                        response["status"] = "success"
                else:
                    cell_to_upd = {
                        "id": id,
                        "type": item['type'],
                        "geometry": item['geometry'],
                        "grid_id": item['properties']['grid_id'],
                        "name": item['properties']['name'],
                        "last_update": datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S"),
                    }
                    cell_upd = db_client.update(data=cell_to_upd, by_set={"_id":id})
                    if cell_upd.modified_count > 0:
                        response["status"] = "success"
            else:
                response["error"] = "The polygon has an wrong geometry"
        else:
            response = problem(403, "Forbidden", "Forbidden")
    else:
        response = problem(401, "Unauthorized", "Unauthorized")
    return response


def patch_cell_by_id(id:str) -> dict:
    """
    Update an existing cell
    :rtype: dict
    """
    isauth = get_apikey()
    if isauth == True:
        if connexion.request.headers['X-Api-Key'] == fd_apikey:
            request = connexion.request.form
            data = {}
            for item in request:
                data[item] = request[item]
                if item == "coordinates":
                    coordinates = request[item].replace("[","").replace("]","").split(",")
                    if len(coordinates) == 10:
                        data[item] = []
                        polygon = []
                        data[item].append(polygon)
                        i = 0
                        while i < 10:
                            polygon.append([float(coordinates[i]), float(coordinates[i+1])])
                            i += 2
                        coordinates_for_validate = {
                            "type": "Polygon",
                            "coordinates": data[item]
                        }
                        isvalid = is_valid(coordinates_for_validate)
                        if isvalid:
                            data['geometry'] = coordinates_for_validate
                            data.pop('coordinates')
                        else:
                            return problem(400, "Bad request", "Polygon has a wron geometry")
                    else:
                        return problem(400, "Bad request", "Coordinates should contain 5 items with longitude and latitude")
            ifexist = db_client.count_document({"_id": id})
            response = {
                "id": id,
                "status": "success"
            }
            if ifexist > 0:
                data['last_update'] = datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S")
                cell_upd = db_client.update(data, {"_id": id})
                if cell_upd.modified_count > 0:
                    return response
                else:
                    response['status'] = "failure"
                    response['description'] = "Something went wrong"
            else:
                response['status'] = "failure"
                response['description'] = "Cell with defined id doesn't exist"
        else:
            response = problem(403, "Forbidden", "Forbidden")
    else:
        response = problem(401, "Unauthorized", "Unauthorized")
    return response


def delete_cell_by_id(id:str) -> dict:
    """
    Delete an existing cell
    :rtype: dict
    """
    isauth = get_apikey()
    if isauth == True:
        if connexion.request.headers['X-Api-Key'] == fd_apikey:
            response = {"id": id}
            start_count = db_client.count_document({"_id":id})
            db_client.delete({"_id": id})
            end_count = db_client.count_document({"_id":id})
            if start_count > end_count:
                response['status'] = 'success'
            else:
                response['status'] = 'failure'
                response['description'] = "Something went wrong"
        else:
            response = problem(403, "Forbidden", "Forbidden")
    else:
        response = problem(401, "Unauthorized", "Unauthorized")
    return response


def get_cell_image(id:str) -> dict:
    """
    Get an image of an ice map cell
    :rtype: dict
    """
    isauth = get_apikey()
    if isauth == True:
        cell = db_client.read({"_id": id})
        response = {
            "image": cell['image'],
            "md5sum": cell['md5sum']
        }
    else:
        response = problem(401, "Unauthorized", "Unauthorized")
    return response


def get_last_update(id:str) -> dict:
    """
    Get the last update time of the cell
    :rtype: dict
    """
    isauth = get_apikey()
    if isauth == True:
        cell = db_client.read({"_id": id})
        response = {
            "id": id,
            "last_update": cell['last_update'],
        }
    else:
        response = problem(401, "Unauthorized", "Unauthorized")
    return response

