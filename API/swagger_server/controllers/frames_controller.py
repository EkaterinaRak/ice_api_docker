import connexion
from datetime import datetime
from swagger_server.orm import OrmMongodb
from swagger_server.validator.geojson_validation import is_valid
from swagger_server.controllers.problem import problem
from swagger_server.controllers.main import get_apikey
from config import fd_apikey


database = "collections"
collection = "frames"
db_client = OrmMongodb(collection=collection, database=database)


def get_cells_frames_by_id(cell_ids: list) -> dict:
    frames_in_grid = []
    for cell_id in cell_ids:
        frames = db_client.read({"cell_id": cell_id}, many=True)
        for frame in frames:
            frame = {
                "type": frame['type'],
                "geometry": frame['geometry'],
                "properties": {
                    "cell_id":frame['cell_id'],
                    "scene_id":frame['scene_id'],
                    "esa_product_id":frame['esa_product_id'],
                    "platform":frame['platform'],
                    "acquired":frame['acquired'],
                    "mode":frame['mode'],
                    "resolution":frame['resolution'],
                    "polarization":frame['polarization'],
                }
            }
            frames_in_grid.append(frame)
    response = {
        "type":"FeatureCollection",
        "features": frames_in_grid
    }
    return response


def get_cell_frames_by_id(id:str) -> dict:
    """
    Get cell frames
    :rtype: dict
    """
    isauth = get_apikey()
    if isauth == True:
        frames = db_client.read({"cell_id": id}, many=True)
        frames_in_cell = []
        for frame in frames:
            frame = {
                "type": frame['type'],
                "geometry": frame['geometry'],
                "properties": {
                    "cell_id": frame['cell_id'],
                    "scene_id": frame['scene_id'],
                    "esa_product_id": frame['esa_product_id'],
                    "platform": frame['platform'],
                    "acquired": frame['acquired'],
                    "mode": frame['mode'],
                    "resolution": frame['resolution'],
                    "polarization": frame['polarization'],
                }
            }
            frames_in_cell.append(frame)
        response = {
            "type": "FeatureCollection",
            "features": frames_in_cell
        }
    else:
        response = problem(401, "Unauthorized", "Unauthorized")
    return response


def post_cell_frames_by_id(id:str) -> dict:
    """
    Add or update frames for a cell
    :rtype: dict
    """
    isauth = get_apikey()
    if isauth == True:
        if connexion.request.headers['X-Api-Key'] == fd_apikey:
            frames_in_request = connexion.request.json['features']
            response_list = []
            response = {}
            for frame in frames_in_request:
                response['id'] = frame['properties']['cell_id']
                isvalid = is_valid(frame)
                count = db_client.count_document({"_id": frame['properties']['scene_id']})
                if isvalid:
                    frame_to_add = {
                        "type": frame['type'],
                        "geometry": frame['geometry'],
                        "scene_id": frame['properties']['scene_id'],
                        "cell_id": frame['properties']['cell_id'],
                        "esa_product_id": frame['properties']['esa_product_id'],
                        "platform": frame['properties']['platform'],
                        "acquired": frame['properties']['acquired'],
                        "mode": frame['properties']['mode'],
                        "resolution": frame['properties']['resolution'],
                        "polarization": frame['properties']['polarization'],
                    }
                    if count == 0:
                        frame_to_add["_id"] = frame['properties']['scene_id']
                        db_client.insert(frame_to_add)
                        end_count = db_client.count_document({"_id":frame['properties']['scene_id']})
                        if end_count > count:
                            response["status"] = "success"
                            response_list.append(response)
                        else:
                            response["status"] = "failure"
                            response["description"] = "Something went wrong"
                            response_list.append(response)
                    else:
                        frame_upd = db_client.update(data=frame_to_add, by_set={"_id": frame['properties']['scene_id']})
                        if frame_upd.modified_count > 0:
                            response["status"] = "success"
                            response_list.append(response)
                else:
                    response["status"] = "failure"
                    response["description"] = "The Polygon has a wrong geometry"
                    return problem(400, 'Bad request', response)
        else:
            response = problem(403, "Forbidden", "Forbidden")
    else:
        response = problem(401, "Unauthorized", "Unauthorized")
    return response


def delete_cell_frames_by_id(id:str) -> dict:
    """
    Clear cell frames
    :rtype: dict
    """
    isauth = get_apikey()
    if isauth == True:
        if connexion.request.headers['X-Api-Key'] == fd_apikey:
            response = {
                "id": id,
                "status": "success"
            }
            start_count = db_client.count_document({"cell_id": id})
            db_client.delete({"cell_id": id}, many=True)
            end_count = db_client.count_document({"cell_id": id})
            if end_count < start_count and end_count == 0:
                return response
            else:
                response["status"] = "failure"
                response["description"] = "Something went wrong"
        else:
            response = problem(403, "Forbidden", "Forbidden")
    else:
        response = problem(401, "Unauthorized", "Unauthorized")
    return response
