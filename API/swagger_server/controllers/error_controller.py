def grids_error_controller(errors_list:list, data:dict) -> list:
    """
    Return description of error
    :rtype: dict
    """
    response_summary = []
    for item in data:
        for error in errors_list:
            if error not in item.keys():
                response = {
                    "id": item['id'] if 'id' in item.keys() else None,
                    "status": "failure",
                    "description": "The request parameter 'grids' must contain the field '%s'" % error
                }
        response_summary.append(response)
    return response_summary


def grid_error_controller(errors_list:list, data:dict) -> dict:
    """
    Return description of error
    :rtype: dict
    """
    for error in errors_list:
        response = {
            "id": data,
            "status": "failure",
            "description": "The request parameter 'grid' must contain the field '%s'" % error
        }
    return response


def cell_error_controller(errors_list:list, id:str) -> list:
    """
    Return description of error
    :rtype: dict
    """
    response_list = []
    for error in errors_list:
        response = {
            "id": id,
            "status": "failure",
            "description": "The request parameter 'cell' must contain the field '%s'" % error
        }
        response_list.append(response)
    return response_list


def cells_error_controller(errors_list:list, data:dict) -> list:
    """
    Return description of error
    :rtype: dict
    """
    response_summary = []
    for item in data['features']:
        for error in errors_list:
            if error not in item.keys():
                response = {
                    "id": item['properties']['id'] if "id" in item['properties'].keys() else None,
                    "status": "failure",
                    "description": "The request parameter 'cell' must contain the field '%s'" % error
                }
                response_summary.append(response)
    return response_summary


def frames_error_controller(errors_list:list, data:dict)-> list:
    """
    Return description of error
    :rtype: dict
    """
    response_summary = []
    for item in data['features']:
        for error in errors_list:
            if error not in item.keys():
                response = {
                    "id": item['properties']['id'] if "id" in item['properties'].keys() else None,
                    "status": "failure",
                    "description": "The request parameter 'frame' must contain the field '%s'" % error
                }
                response_summary.append(response)
    return response_summary
