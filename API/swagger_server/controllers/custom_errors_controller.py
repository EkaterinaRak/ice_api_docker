from connexion.decorators.validation import RequestBodyValidator
from connexion.utils import is_null
from jsonschema.validators import validator_for
from swagger_server.controllers.problem import problem
from swagger_server.controllers.error_controller import *


class CustomRequestBodyValidator(RequestBodyValidator):
    def __init__(self, schema, consumes, api, is_null_value_valid=False, validator=None,
                 strict_validation=False):
        """
        :param schema: The schema of the request body
        :param consumes: The list of content types the operation consumes
        :param is_null_value_valid: Flag to indicate if null is accepted as valid value.
        :param validator: Validator class that should be used to validate passed data
                          against API schema. Default is jsonschema.Draft4Validator.
        :type validator: jsonschema.IValidator
        :param strict_validation: Flag indicating if parameters not in spec are allowed
        """
        self.consumes = consumes
        self.schema = schema
        self.has_default = schema.get('default', False)
        self.is_null_value_valid = is_null_value_valid

    def validate_schema(self, data:dict, url:str) ->dict:
        response_list = []
        errors_list =[]
        if self.is_null_value_valid and is_null(data):
            return None
        cls = validator_for(self.schema)
        cls.check_schema(self.schema)
        errors = tuple(cls(self.schema).iter_errors(data))
        if errors:
            for error in errors:
                error_check = error.message.split(" ")
                if "required" in error_check:
                        errors_list.append(error.message.split("'")[1])
            if "grids" in url.split('/'):
                response = grids_error_controller(errors_list, data)
            if "grid" in url.split('/'):
                response = grid_error_controller(errors_list, url.split('/')[-1])
            if "cells" in url.split('/'):
                response = cells_error_controller(errors_list, data)
            if "cell" in url.split('/') and "frames" not in url.split('/'):
                response = cell_error_controller(errors_list, url.split('/')[-1])
            if "cell" in url.split('/') and "frames" in url.split('/'):
                response = frames_error_controller(errors_list, data)
            return problem(400, 'Bad Request', response, type='validation')
        return None



