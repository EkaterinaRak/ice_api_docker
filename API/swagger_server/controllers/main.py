import connexion
import requests
from config import fd_apikey
from swagger_server.controllers.problem import problem


def ping() -> bool:
    return True


def get_apikey() -> bool:
    args = connexion.request.headers
    if 'X-Api-Key' in args.keys():
        apikey = args['X-Api-Key']
        if apikey == fd_apikey:
            return True
        url = "http://maps.kosmosnimki.ru/rest/ver1/service/wms?map=ATTBP&bbox=66.797497,40.088867,67.867264,42.758789&CRS=EPSG:4326&WIDTH=837&HEIGHT=561&FORMAT=image/jpeg&Layers=AF64ECA6B32F437CB6AC72B5E6F85B97&request=getmap&hoursOffset=72&apikey=%s" % (apikey)
        response = requests.get(url)
        if "Exception" in response.text:
            return problem(401, "Unauthorized", "Unauthorized")
        return True
    else:
        return problem(401, "Unauthorized", "Unauthorized")





