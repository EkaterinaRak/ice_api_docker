import connexion
from datetime import datetime
from swagger_server.orm import OrmMongodb
from swagger_server.controllers.problem import problem
from swagger_server.controllers.main import get_apikey
from config import fd_apikey

database = "collections"
collection = "grids"
db_client = OrmMongodb(collection=collection, database=database)


def get_grids() -> list:
    """
    Return a list of all grids
    :rtype: list
    """
    isauth = get_apikey()
    if isauth == True:
        response = []
        grids_in_db = db_client.read(data={}, many=True)
        for item in grids_in_db:
            grid = {
                "id": item['id'],
                "name": item['name'],
                "image": item['image'],
                "description": item['description'],
                "created": item['created'],
                "updated": item['updated'],
            }
            response.append(grid)
    else:
        response = problem(401, "Unauthorized", "Unauthorized")
    return response


def get_grid_by_id(id:str) -> dict:
    """
    Return grid by id
    :rtype: list
    """
    isauth = get_apikey()
    if isauth == True:
        item = db_client.read(data={"_id":id})
        grid = {
            "id": item['id'],
            "name": item['name'],
            "image": item['image'],
            "description": item['description'],
            "created": item['created'],
            "updated": item['updated'],
        }
        return grid
    else:
        response = problem(401, "Unauthorized", "Unauthorized")
    return response


def post_grids() -> list:
    """
    Return a list of added grids in database
    :rtype: list
    """
    isauth = get_apikey()
    if isauth == True:
        if  connexion.request.headers['X-Api-Key'] == fd_apikey:
            items_id = []
            response_list = []
            for item in connexion.request.json:
                items_id.append(item['id'])
                count = db_client.count_document(data={"_id": item['id']})
                if count > 0:
                    grid_to_upd = {
                        "id": item['id'],
                        "name": item['name'],
                        "image": item['image'],
                        "description": item['description'],
                        "updated": datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S")
                    }
                    db_client.update(data = grid_to_upd, by_set={"_id": grid_to_upd['id']})
                else:
                    grid_to_add = {
                        "_id": item['id'],
                        "id": item['id'],
                        "name": item['name'],
                        "image": item['image'],
                        "description": item['description'],
                        "created": datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S"),
                        "updated": datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S")
                    }
                    db_client.insert(grid_to_add)
            for item_id in items_id:
                response_list.append(get_grid_by_id(item_id))
            return response_list
        else:
            response = problem(403, "Forbidden", "Forbidden")
            return response
    else:
        response = problem(401, "Unauthorized", "Unauthorized")
        return response


def delete_grids() -> list:
    """
    Return a list of dicts with operation status
    :rtype: list
    """
    isauth = get_apikey()
    if isauth == True:
        if  connexion.request.headers['X-Api-Key'] == fd_apikey:
            grids_id = []
            response = []
            grids_in_db = get_grids()
            for grid_in_db in grids_in_db:
                grids_id.append(grid_in_db['id'])
                db_client.delete(data={"_id":grid_in_db['id']})
                count = db_client.count_document({"_id": grid_in_db['id']})
                if count == 0:
                    grid = {
                        "id": grid_in_db['id'],
                        "status": "success"
                    }
                else:
                    grid = {
                        "id": grid_in_db['id'],
                        "status": "failure",
                        "description":"Something went wrong"
                    }
                response.append(grid)
        else:
            response = problem(403, "Forbidden", "Forbidden")
    else:
        response = problem(401, "Unauthorized", "Unauthorized")
    return response


def put_grid_by_id(id) -> dict:
    """
    Add new grid with defined id
    :rtype: dict
    """
    isauth = get_apikey()
    if isauth == True:
        if connexion.request.headers['X-Api-Key'] == fd_apikey:
            grid_to_add = connexion.request.json
            grid_to_add['updated'] = datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S")
            start_count = db_client.count_document({"_id":id})
            if start_count == 0:
                grid_to_add['created'] = datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S")
                grid_to_add['_id'] = id
                db_client.insert(grid_to_add)
                end_count = db_client.count_document({"_id": id})
                if start_count < end_count:
                    response = {
                        "id": id,
                        "status": "success"
                    }
                else:
                    response = {
                        "id":id,
                        "status":"failure",
                        "description": "Something went wrong"
                    }
            else:
                grid_upd = db_client.update(data=grid_to_add, by_set={"_id":id})
                if grid_upd.modified_count > 0:
                    response = {
                        "id": id,
                        "status":"success"
                    }
                else:
                    response = {
                        "id": id,
                        "status": "failure",
                        "description": "Something went wrong"
                    }
        else:
            response = problem(403, "Forbidden", "Forbidden")
    else:
        response = problem(401, "Unauthorized", "Unauthorized")
    return response


def patch_grid_by_id(id) -> dict:
    """
    Update an existing grid
    :rtype: dict
    """
    isauth = get_apikey()
    if isauth == True:
        if connexion.request.headers['X-Api-Key'] == fd_apikey:
            grid_to_add = connexion.request.json
            grid_to_add['updated'] = datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S")
            start_count = db_client.count_document({"_id":id})
            if start_count == 0:
                grid_to_add['created'] = datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S")
                grid_to_add['_id'] = id
                db_client.insert(grid_to_add)
                end_count = db_client.count_document({"_id": id})
                if start_count < end_count:
                    response = {
                        "id": id,
                        "status": "success"
                    }
                else:
                    response = {
                        "id":id,
                        "status":"failure",
                        "description": "Something went wrong"
                    }
            else:
                grid_upd = db_client.update(data=grid_to_add, by_set={"_id":id})
                if grid_upd.modified_count > 0:
                    response = {
                        "id": id,
                        "status":"success"
                    }
                else:
                    response = {
                        "id": id,
                        "status": "failure",
                        "description": "Something went wrong"
                    }
            return response
        else:
            response = problem(403, "Forbidden", "Forbidden")
    else:
        response = problem(401, "Unauthorized", "Unauthorized")
    return response


def delete_grid_by_id(id) -> dict:
    """
    Delete an existing grid
    :rtype: dict
    """
    isauth = get_apikey()
    if isauth == True:
        if connexion.request.headers['X-Api-Key'] == fd_apikey:
            db_client.delete(data={"_id": id})
            count = db_client.count_document({"_id": id})
            if count == 0:
                response = {
                    "id": id,
                    "status": "success"
                }
            else:
                response = {
                    "id": id,
                    "status": "failure",
                    "description": "Something went wrong"
                }
        else:
            response = problem(403, "Forbidden", "Forbidden")
    else:
        response = problem(401, "Unauthorized", "Unauthorized")
    return response



