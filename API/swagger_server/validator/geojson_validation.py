from shapely.geometry import shape


def is_valid(geometry: dict):
    """
    Geojson validation
    """
    if geometry.get('type') == 'FeatureCollection':
        features = geometry.get('features', [])
        geometries = (shape(feature.get('geometry')).is_valid for feature in features)
        if all(geometries) is True:
            return True
    elif geometry.get('type') == 'Feature':
        geometry = geometry.get('geometry')
        shapely_geometry = shape(geometry)
        return shapely_geometry.is_valid
    else:
        shapely_geometry = shape(geometry)
        return shapely_geometry.is_valid
