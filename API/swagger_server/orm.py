"""
ORM for MongoDB drivers implemented on the basis of PyMongo.
"""
from typing import List, Union
from pymongo import MongoClient, ReturnDocument
from pymongo.errors import ServerSelectionTimeoutError
from config import DatabaseConfiguration
from swagger_server.constants import MongodbConstants
const = DatabaseConfiguration



class OrmMongodb:
    """
    CRUD model.
    Class for working with mongodb via pymongo.
    """
    def __init__(self, sockets: list=None, replica_set: str=None, database: str=None, collection: str=None, user=None, password=None):
        self.sockets = MongodbConstants.SOCKETS
        self.replica_set = replica_set
        self.database = database
        self.collection = collection
        self.user = user
        self.password = password
        self.__client = self.__get_client()

    def __get_client(self):
        """
        Client for a MongoDB instance, a replica set, or a set of mongoses.
        """
        return MongoClient(self.sockets, replicaset=self.replica_set, username=self.user, password=self.password)

    def __get_crud_object(self):
        """
        Internal object for crud model
        """
        return self.__client[self.database][self.collection]

    def __set_connection(self):
        """
        Attempt to connect to a writable server.
        """
        return self.__client.server_info()

    def _read(self, many: bool):
        """
        Internal read helper.
        """
        if many is False:
            return self.__get_crud_object().find_one
        elif many is True:
            return self.__get_crud_object().find
        else:
            pass

    def read(self, data: dict, many: bool=False):
        """
        Get a single or multiple documents from the database.
        """
        self.__set_connection()

        action = self._read(many)
        if isinstance(data, dict):
            db_data = self.__confirm_action(action, data)
            if isinstance(db_data, dict):
                return db_data
            else:
                return list(db_data)
        else:
            pass

    def _count_document(self, data: dict):
        if isinstance(data, dict):
            return self.__get_crud_object().count_documents
        else:
            pass

    def count_document(self, data: dict):
        """
        Count documents in database collection by query
        :param data: dictionary
        """
        self.__set_connection()
        action = self._count_document(data)
        return self.__confirm_action(action, data)

    def _insert(self, data: Union[dict, List[dict]]):
        """
        Internal insert helper.
        """
        self._is_primary()

        if isinstance(data, dict):
            return self.__get_crud_object().insert_one
        elif all(map(lambda element: isinstance(element, dict), data)) and data:
            return self.__get_crud_object().insert_many
        else:
            pass

    def insert(self, data: Union[dict, List[dict]]):
        """
        Insert single and iterable of documents.
        Available only for primary replica.
        :param data: dictionary or list of dictionaries
        """
        self.__set_connection()

        action = self._insert(data)
        return self.__confirm_action(action, data)

    def _update(self, many: bool):
        """
        Internal update helper.
        """
        self._is_primary()

        if many is False:
            return self.__get_crud_object().update_one
        elif many is True:
            return self.__get_crud_object().update_many
        else:
            pass

    def update(self, data: dict, by_set: dict, many: bool=False):
        """
        Update a single or multiple documents matching the by_set.
        Available only for primary replica.
        """
        self.__set_connection()

        action = self._update(many)
        if isinstance(data, dict) and isinstance(by_set, dict):
            return self.__confirm_action(action, by_set, {const.operator_expression: data})
        else:
            pass

    def _increment(self, many: bool):
        """
        Internal update helper.
        """
        self._is_primary()

        if many is False:
            return self.__get_crud_object().update_one
        elif many is True:
            return self.__get_crud_object().update_many
        else:
            pass

    def increment(self, data: dict, by_set: dict, many: bool = False):
        """
        Update a single or multiple documents matching the by_set.
        Available only for primary replica.
        """
        self.__set_connection()

        action = self._update(many)
        if isinstance(data, dict) and isinstance(by_set, dict):
            return self.__confirm_action(action, by_set, {const.operator_increment: data})
        else:
            pass


    def _delete(self, many: bool):
        """
        Internal delete helper.
        """
        self._is_primary()

        if many is False:
            return self.__get_crud_object().delete_one
        elif many is True:
            return self.__get_crud_object().delete_many
        else:
            pass

    def delete(self, data: dict, many: bool=False):
        """
        Delete one or more documents matching the data.
        Available only for primary replica.
        """
        self.__set_connection()

        action = self._delete(many)
        if isinstance(data, dict):
            return self.__confirm_action(action, data)
        else:
            pass

    @staticmethod
    def __confirm_action(action, data: Union[dict, List[dict]], by_set: dict=None):
        """
        Internal CRUD helper.
        """
        try:
            if by_set is not None:
                return action(data, by_set)
            else:
                return action(data)
        except ServerSelectionTimeoutError:
            pass
        except TypeError:
            pass

    def _is_primary(self):
        """
        If this client is connected to a server that can accept writes.
        """
        primary = self.__client.is_primary
        if primary is not True:
            raise AssertionError(const.replicaset_primary_error)
        return primary

    def close_client(self):
        """
        Cleanup client resources and disconnect from MongoDB.
        """
        return self.__client.close()
