#!/usr/bin/env python3
import connexion
from swagger_server import encoder
from swagger_server.controllers.custom_errors_controller import CustomRequestBodyValidator


def main():
    app = connexion.App(__name__, specification_dir='./swagger/',  strict_validation=True, validator_map={'body': CustomRequestBodyValidator})
    app.app.json_encoder = encoder.JSONEncoder
    app.add_api('swagger.yaml', arguments={'title': 'Ice Chart Service'})
    app.run(port=3000)
    app.debug = True


if __name__ == '__main__':
    main()