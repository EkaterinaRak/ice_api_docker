"""
Project's constants
"""
import os
import sys
from dotenv import load_dotenv, dotenv_values

load_dotenv()
parsed = dotenv_values()
argsdict = {}
for farg in sys.argv:
    if farg.startswith('--'):
        (arg, val) = farg.split("=")
        arg = arg[2:]

        if arg in argsdict:
            argsdict[arg].append(val)
        else:
            argsdict[arg] = [val]
if argsdict and argsdict.get('ENV') and argsdict.get('ENV')[0] == 'dev':
    env = 'DEV_'
else:
    env = ''


class MongodbConstants:
    """
    SOCKETS - list or tuple of strings
    REPLICA_SET, DATABASE, COLLECTION, USER, PASSWORD - string
    FOR EXAMPLE
    SOCKETS = ['0.0.0.0:27017', '0.0.0.0:27018', '0.0.0.0:27019']
    REPLICA_SET = 'replica_name'
    DATABASE = 'database_name'
    COLLECTION = 'collection_name'
    USER = 'root'
    PASSWORD = 'password'
    """
    SOCKETS = argsdict.get('DBC_SOCKETS') if argsdict.get('DBC_SOCKETS') else parsed['%sDBC_SOCKETS' % env]
    REPLICA_SET = argsdict.get('DBC_REPLICA_SET')[0] if argsdict.get('DBC_REPLICA_SET') else parsed[
        '%sDBC_REPLICA_SET' % env]
    DATABASE = argsdict.get('DBC_DATABASE')[0] if argsdict.get('DBC_DATABASE') else parsed['%sDBC_DATABASE' % env]
    COLLECTION = argsdict.get('DBC_COLLECTION')[0] if argsdict.get('DBC_COLLECTION') else parsed[
        '%sDBC_COLLECTION' % env]
    USER = argsdict.get('DBC_USER')[0] if argsdict.get('DBC_USER') else parsed['%sDBC_USER' % env]
    PASSWORD = argsdict.get('DBC_PASSWORD')[0] if argsdict.get('DBC_PASSWORD') else parsed['%sDBC_PASSWORD' % env]


class MetaDataBaseConstants:
    """
    SOCKETS - list or tuple of strings
    REPLICA_SET, DATABASE, COLLECTION, USER, PASSWORD - string
    FOR EXAMPLE
    SOCKETS = '0.0.0.0:27017', '0.0.0.0:27018', '0.0.0.0:27019'
    REPLICA_SET = 'replica_name'
    DATABASE = 'database_name'
    COLLECTION = 'collection_name'
    USER = 'root'
    PASSWORD = 'password'
    """

    SOCKETS = argsdict.get('MDBC_SOCKETS') if argsdict.get('MDBC_SOCKETS') else parsed['%sMDBC_SOCKETS' % env]
    REPLICA_SET = argsdict.get('MDBC_REPLICA_SET')[0] if argsdict.get('MDBC_REPLICA_SET') else parsed[
        '%sMDBC_REPLICA_SET' % env]
    DATABASE = argsdict.get('MDBC_DATABASE')[0] if argsdict.get('MDBC_DATABASE') else parsed['%sMDBC_DATABASE' % env]
    COLLECTION = argsdict.get('MDBC_COLLECTION')[0] if argsdict.get('MDBC_COLLECTION') else parsed[
        '%sMDBC_COLLECTION' % env]
    USER = argsdict.get('MDBC_USER')[0] if argsdict.get('MDBC_USER') else parsed['%sMDBC_USER' % env]
    PASSWORD = argsdict.get('MDBC_PASSWORD')[0] if argsdict.get('MDBC_PASSWORD') else parsed['%sMDBC_PASSWORD' % env]


class SourceConstants:
    """
    API_URL - basic api url
    API_VERSION - version
    FOR EXAMPLE
    API_URL = 'http://api.example.com/dev'
    API_VERSION = 'v1'
    """
    API_URL = argsdict.get('SOURCE_API_URL')[0] if argsdict.get('SOURCE_API_URL') else parsed['%sSOURCE_API_URL' % env]
    API_VERSION = argsdict.get('SOURCE_API_VERSION')[0] if argsdict.get('SOURCE_API_VERSION') else parsed[
        '%sSOURCE_API_VERSION' % env]


class ApiManagerConstants:
    """
    API_URL - basic api url
    API_KEY - secret api key
    FOR EXAMPLE
    API_URL = 'http://api.example.com/swagger'
    API_KEY = '9c7fdac74cadd6569957f0efe8576a2a'
    """
    API_URL = argsdict.get('CRED_API_URL')[0] if argsdict.get('CRED_API_URL') else parsed['%sCRED_API_URL' % env]
    API_KEY = argsdict.get('CRED_API_KEY')[0] if argsdict.get('CRED_API_KEY') else parsed['%sCRED_API_KEY' % env]


class LoggerConstants:
    """
    SOCKETS - list or tuple of strings
    REPLICA_SET, DATABASE, COLLECTION_FD, COLLECTION_API, COLLECTION_SPACE_BASE, COLLECTION_SERVICE_INFORMATION,
     USER, PASSWORD - string
    FOR EXAMPLE
    SOCKETS = ['0.0.0.0:27017', '0.0.0.0:27018', '0.0.0.0:27019']
    REPLICA_SET = 'replica_name'
    DATABASE = 'database_name'
    COLLECTION_FD = 'collection_name_for_FD'
    COLLECTION_API = 'collection_name_for_API'
    COLLECTION_SPACE_BASE = 'collection_name_for_SPACE_BASE'
    COLLECTION_SERVICE_INFORMATION = 'collection_SERVICE_INFORMATION'
    USER = 'root'
    PASSWORD = 'password'
    """
    BASE_DIR = os.path.abspath(os.path.join(__file__, os.pardir))
    SOCKETS = argsdict.get('LOGER_SOCKETS') if argsdict.get('LOGER_SOCKETS') else parsed['%sLOGER_SOCKETS' % env]
    REPLICA_SET = argsdict.get('LOGER_REPLICA_SET')[0] if argsdict.get('LOGER_REPLICA_SET') else parsed[
        '%sLOGER_REPLICA_SET' % env]
    DATABASE = argsdict.get('LOGER_DATABASE')[0] if argsdict.get('LOGER_DATABASE') else parsed['%sLOGER_DATABASE' % env]
    COLLECTION_FD = argsdict.get('LOGER_COLLECTION_FD')[0] if argsdict.get('LOGER_COLLECTION_FD') else parsed[
        '%sLOGER_COLLECTION_FD' % env]
    COLLECTION_API = argsdict.get('LOGER_COLLECTION_API')[0] if argsdict.get('LOGER_COLLECTION_API') else parsed[
        '%sLOGER_COLLECTION_API' % env]
    COLLECTION_SPACE_BASE = argsdict.get('LOGER_COLLECTION_SPACE_BASE')[0] if argsdict.get(
        'LOGER_COLLECTION_SPACE_BASE') else parsed['%sLOGER_COLLECTION_SPACE_BASE' % env]
    COLLECTION_SERVICE_INFORMATION = argsdict.get('LOGER_COLLECTION_SERVICE_INFORMATION')[0] if argsdict.get(
        'LOGER_COLLECTION_SERVICE_INFORMATION') else parsed['%sLOGER_COLLECTION_SERVICE_INFORMATION' % env]
    USER = argsdict.get('LOGER_USER')[0] if argsdict.get('LOGER_USER') else parsed['%sLOGER_USER' % env]
    PASSWORD = argsdict.get('LOGER_PASSWORD')[0] if argsdict.get('LOGER_PASSWORD') else parsed['%sLOGER_PASSWORD' % env]


class SetupDaemonConstants:
    """
    INTERVAL_RUN_IC_FD_SEC - string
    INTERVAL_PING_API_SEC - string

    FOR EXAMPLE:
    INTERVAL_RUN_IC_FD_SEC = '10'
    INTERVAL_PING_API_SEC = '3'
    """

    INTERVAL_RUN_IC_FD_SEC = argsdict.get('INTERVAL_RUN_IC_FD_SEC')[0] if argsdict.get('INTERVAL_RUN_IC_FD_SEC') else \
    parsed['%sINTERVAL_RUN_IC_FD_SEC' % env]
    INTERVAL_RUN_PING_API_SEC = argsdict.get('INTERVAL_RUN_PING_API_SEC')[0] if argsdict.get(
        'INTERVAL_RUN_PING_API_SEC') else parsed['%sINTERVAL_RUN_PING_API_SEC' % env]


class MetaDataKeysConstants:
    """
    Constant keys from meta parameters database
    """
    LAYER = 'layer'
    CRS = 'crs'
    KEY = 'key'
    MAP = 'map'
    REQUEST = 'request'
    WIDTH = 'width'
    HEIGHT = 'height'
    FORMAT = 'format'
    HOURS_OFFSET = 'hours_offset'
