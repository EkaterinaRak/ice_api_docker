**IC_API**

_Python version_

version 3.5 and up.

_Library requirements_
- swagger
- flask 
- Connexion
- jsonschema
- bson
- pymongo
- requests
- python_dateutil 
- setuptools 
- config
- shapely

_Common description of API_

The service provides data transfer in a user-friendly and computer-friendly format (JSON), implementing the correct execution of data acquisition procedures through the application's endpoint (final addresses for requests).
The service is always waiting for a request to endpoint
Upon receiving a request, the service should authenticate the user and, if successful, give him a relevant response.

_Status_codes and description_

200 - Ok 
400 - Bad request 
401 - Unauthorized 
403 - Forbidden  
404 - Not found 
500 - Internal server error 


_Method's description_

/grids:

    -GET:
        get a list of existing grids.
    -POST: 
        Add or update grids
        Required parameters: 
          -id: grid's ID in "UUID4" format
          -name: name
          -description: description 
          -image: grid's image({ "width": "3000px", "height": "1000px" })
    -DELETE:
        delete all existing grids.
        
/grid/{id}:

    -GET:
        get a grid with defined id.
    -PUT: 
        add new grid.
        Required parameters: 
          -id: grids ID in "UUID4" format
          -name: name 
          -description: description 
          -image: grid's image({ "width": "3000px", "height": "1000px" })
    -PATCH: 
        update an existing grid.
        Required parameters: 
          -id: grid's ID in "UUID4" firmat
          -name: name 
          -description: description 
          -image: grid's image({ "width": "3000px", "height": "1000px" })
    -DELETE:
        delete grid with defined id.
        
/cells/{id}:

      -GET:
            get a list of cells in grid with defined id.
      -POST: 
            add or update cells in grid with defined id: 
            -type: "FeatureCollection"
            -feature:
                  -type
                  -geometry
                  -properties:
                    -id: cell's id
                    -name: cell's name
      -DELETE:
            delete all existing cells in the grid.
            
/cells/frames/{id}:

      -GET:
           get a list of frames of all cells ini grid with defined id.          
            
/cell/{id}:

      -GET:
            get a cell with defined id.
      -PUT: 
            add new cell with defined id.
            Required parameters: 
            -type: "FeatureCollection"
            -feature:
                  -type
                  -geometry
                  -properties:
                    -id: cell's id
                    -name: cell's name
      -PATCH: 
            update an existing cell.
            Required parameters: 
            -type: "FeatureCollection"
            -feature:
                  -type
                  -geometry
                  -properties:
                      -id: cell's id
                      -name: cell's name
      -DELETE:
            delete cell.         
            
/cell/frames/{id}:

      -GET:
            get a list of frames in cell with defined id.
      -POST: 
            add or update frames in cell with defined id
            Required parameters: 
            -type: "FeatureCollection"
            -feature:
                  -type
                  -geometry
                  -properties:
                     -cell_id: cell's id 
                     -scene_id: "S1B_EW_GRDM_1SDH_20180725T022404_1"
                     -esa_product_id: "4dd5296a-4dc6-4f4f-b5ce-50ee73f4c8b9"
                     -platform: "Sentinel-1B",
                     -acquired: "2018-07-25T14:02:24:04Z",
                     -mode: "EW",
                     -resolution: 40,
                     -polarization: "HH"
      -DELETE:
            delete all existing frames in the cell.
            
/cell/image/{id}:

      -GET:
            get cell's image.                        
/cell/last_update/{id}:

      -GET:
            get a date of cell's last update              

/ping:

      -GET:
            return true.  



    



